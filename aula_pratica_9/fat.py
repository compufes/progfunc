def fat(n):
    print("Funcao fat chamada com n = ", n)
    if n <= 1:
        return 1
    else:
        res = n * fat(n-1)
        print("resultado intermediário para {} * fat({}) = {}".format(n, n-1, res))
        return res	

print("5! =",fat(5))