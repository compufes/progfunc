def fatorial(n):
    return 1 if n == 0 else n*fatorial(n-1)

def main():
    n = 3
    print("{}! = {}".format(n, fatorial(n)))
    
main()
