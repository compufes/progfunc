def sn2tf(x):
    return True if x == "S" else False if x == "N" else None

def tf2sn(x):
    return "S" if x == True else "N" if x == False else None

def forma_mensagem(a, b):
    return b if not a else a + '\n' + b

def main():
    mensagem = ""
    doa = True
###

    peso = float(input())
    doa = doa and 50 <= peso
    if not 50 <= peso:
        mensagem = forma_mensagem(mensagem, "Impedimento: abaixo do peso minimo.")

    print("Peso: {}".format(peso))
###

    idade = int(input())
    print("Idade: {}".format(idade))
    doa = doa and 16 <= idade 
    if not 16 <= idade:
        mensagem = forma_mensagem(mensagem, "Impedimento: menor de 16 anos.")
    if idade == 16 or idade == 17:
        consentimento = sn2tf(str(input()))
        print("Documento de autorizacao: {}".format(tf2sn(consentimento)))
        doa = doa and consentimento
        if not consentimento:
            mensagem = forma_mensagem(mensagem, "Impedimento: menor de 18 anos, sem consentimento dos responsaveis.")
    elif 18 <= idade:
        if idade > 60:
            doa = doa and not primeira_doacao
            if primeira_doacao:
                mensagem = forma_mensagem(mensagem, "Impedimento: maior de 60 anos, primeira doacao.")
    doa = doa and idade <= 69
    if idade > 69:
        mensagem = forma_mensagem(mensagem, "Impedimento: maior de 69 anos.")

###

    boa_saude = sn2tf(str(input()))
    print("Boa saude: {}".format(tf2sn(boa_saude)))
    doa = doa and boa_saude
    if not boa_saude:
        mensagem = forma_mensagem(mensagem, "Impedimento: nao esta em boa saude.")

###

    usa_drogas_injetaveis = sn2tf(str(input()))
    print("Uso drogas injetaveis: {}".format(tf2sn(usa_drogas_injetaveis)))
    doa = doa and not usa_drogas_injetaveis
    if usa_drogas_injetaveis:
        mensagem = forma_mensagem(mensagem, "Impedimento: uso de drogas injetaveis.")

###

    primeira_doacao = sn2tf(str(input()))
    print("Primeira doacao: {}".format(tf2sn(primeira_doacao)))
    if not primeira_doacao:
        intervalo_doacoes, maximo_doacoes = \
                int(input()), int(input())
        print("Meses desde ultima doacao: {}".format(intervalo_doacoes))
        print("Doacoes nos ultimos 12 meses: {}".format(maximo_doacoes))
        sexo = str(input())
        if sexo == "M":
            print("Sexo biologico: {}".format(sexo))
            doa = doa and 2 <= intervalo_doacoes
            if not 2 <= intervalo_doacoes:
                mensagem = forma_mensagem(mensagem, "Impedimento: intervalo minimo entre as doacoes nao foi respeitado.")
            doa = doa and maximo_doacoes < 4
            if not maximo_doacoes < 4:
                mensagem = forma_mensagem(mensagem, "Impedimento: numero maximo de doacoes anuais foi atingido.")
        else:
            print("Sexo biologico: {}".format(sexo))
            doa = doa and 3 <= intervalo_doacoes
            if not 3 <= intervalo_doacoes:
                mensagem = forma_mensagem(mensagem, "Impedimento: intervalo minimo entre as doacoes nao foi respeitado.")
            doa = doa and maximo_doacoes < 3
            if not maximo_doacoes < 3:
                mensagem = forma_mensagem(mensagem, "Impedimento: numero maximo de doacoes anuais foi atingido.")

            gravidez = sn2tf(str(input()))
            print("Gravidez: {}".format(tf2sn(gravidez)))
            amamentando = sn2tf(input())
            print("Amamentando: {}".format(tf2sn(amamentando)))

            doa = doa and not gravidez
            if gravidez:
                mensagem = forma_mensagem(mensagem, "Impedimento: gravidez.")
            if amamentando:
                mesesbb = int(input())
                doa = doa and 12 <= mesesbb
                if mesesbb < 12:
                    mensagem = forma_mensagem(mensagem, "Impedimento: amamentacao.")
                print("Meses bebe: {}".format(mesesbb))
    else:
        sexo = str(input())
        if sexo == "M":
            print("Sexo biologico: {}".format(sexo))
        else:
            print("Sexo biologico: {}".format(sexo))
            gravidez = sn2tf(str(input()))
            print("Gravidez: {}".format(tf2sn(gravidez)))
            amamentando = sn2tf(input())
            print("Amamentando: {}".format(tf2sn(amamentando)))

            doa = doa and not gravidez
            if gravidez:
                mensagem = forma_mensagem(mensagem, "Impedimento: gravidez.")
            if amamentando:
                mesesbb = int(input())
                doa = doa and 12 <= mesesbb
                if mesesbb < 12:
                    mensagem = forma_mensagem(mensagem, "Impedimento: amamentacao.")
                print("Meses bebe: {}".format(mesesbb))
    if doa:
        mensagem = "Procure um hemocentro."
    print(mensagem)

main()

