def critica(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        print('Valores invalidos.')
        return
    elif a > (b+c):
        print('Valores nao podem formar um triangulo.')
        return

    if a == b and b == c:
        print('Triangulo equilatero.')
    elif a == b or a == c or b == c:
        print('Triangulo isosceles.')
    else:
        print('Triangulo escaleno.')

    if a**2 < (b**2 + c**2):
        print('Triangulo acutangulo.')
    elif a**2 > (b**2 + c**2):
        print('Triangulo obtusangulo.')
    else:
        print('Triangulo retangulo.')

#
# comeca aqui 
#

a, b, c = float(input()), float(input()), float(input())

if a < b:
    a, b = b, a
if a < c:
    a, c = c, a

# print("Hipo {} Cat {}  Cat {}".format(a, b, c))    

critica(a, b, c)

