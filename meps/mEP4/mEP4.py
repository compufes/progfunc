def outputMensage( \
        nj0, nj1, nj2, \
        nm0, nm1, nm2, \
        bj, bm, \
        d1, d2, \
        hotel, passagem):

    j_aprov = nj0 >= 7 and nj1 >= 7 and nj2 >= 7
    m_aprov = nm0 >= 7 and nm1 >= 7 and nm2 >= 7
    jm_ferias = bj and bm
    salario_pago = d1 <= 15 or d2 <= 15
    menor_q10mil = hotel + passagem <= 10000

    print("Joao aprovado em todas as disciplinas? {}".format(j_aprov))
    print("Maria aprovada em todas as disciplinas? {}".format(m_aprov))
    print("Eduardo e Monica de ferias de dezembro? {}".format(jm_ferias))
    print("Pagamento de Eduardo ou Monica liberado a tempo? {}".format(salario_pago))
    print("Valor total menor ou igual a R$10.000,00? {}".format(menor_q10mil))
    print("Irao viajar? {}".format(j_aprov and m_aprov and jm_ferias and salario_pago and menor_q10mil))

def main():
    nj0, nj1, nj2 = float(input()), float(input()), float(input())
    nm0, nm1, nm2 = float(input()), float(input()), float(input())
    tbj, tbm = str(input()), str(input())

    bj = tbj == "Sim"
    bm = tbm == "Sim"

    d1, d2 = int(input()), int(input())
    hotel, passagem = float(input()), float(input())

    outputMensage( \
        nj0, nj1, nj2, \
        nm0, nm1, nm2, \
        bj, bm, \
        d1, d2, \
        hotel, passagem)
main()

