def paralelepipedo(h, l, c, ch):
    if 0 <= h:
        print(c * " " + ch + l * " " + ch)
        paralelepipedo(h - 1, l, c + 1, ch)
    return c

def reangulo(h, l, ch):
    if 0 <= h:
        print(ch + (l - 2) * " " + ch)
        reangulo(h - 1, l, ch)

def triangTRE(h, ch):
    a = (h - 1) * " " + ch if h else ""
    if 0 <= h:
        h -= 1
        triangTRE(h, ch)
        print(ch + a)

def triangTRD(h0, ch, h1):
    a = (h0 + 1) * " " + (ch if h0 + 2 != h1 else "")
    b = (h1 - h0 - 3) * " " + ch
    if 0 <= h0:
        h0 -= 1
        # h1 += 1
        print(a + b)
        triangTRD(h0, ch, h1)

def triangTE(h0, ch, h1):
    if 0 <= h0:
        print(((h0 + 1) * " ") + ch + (h1 * " ") + (ch if 0 <= h1 else ""))
        triangTE(h0 - 1, ch, h1 + 2)

def desenhaFigura(ob, h, l, ch):
    if ob == "TRE":
        triangTRE(h-2, ch)
        print(h*ch)
    elif ob == "TRD":
        triangTRD(h-2, ch, h)
        print(h*ch)
    elif ob == "TE":
        triangTE(h-2, ch, -1)
        print((2*h-1)*ch)
    elif ob == "R":
        print(l * ch)
        reangulo(h-3, l, ch)
        print(l * ch)
    elif ob == "P":
        print(l * ch)
        r = paralelepipedo(h-3, l - 2, 1, ch)
        print((h - 1) * " " + l * ch)

def main():
    ob = input()
    opc = False
    l = 0
    if ob == "R" or ob == "P":
        l = int(input())
    elif ob == "TE" or ob == "TRE" or ob == "TRD":
        opc = True
    else:
        print("Objeto invalido.")
        exit()
    h = int(input())
    if h <= 0 or (ob == "R" or ob == "P") and l <= 0:
        print("Medida invalida.")
        exit()

    ch = str(input())

    desenhaFigura(ob, h, l, ch)

main()

