x = float(input())
op = input()
y = float(input())

r = 0
if op != "+" and op != "-" and op != "*" and \
   op != "/" and op != "//" and op != "%" and \
   op != "**":
    print("OPERACAO NAO RECONHECIDA")
else:
    if op == "+":
        r = x + y
    elif op == "-":
        r = x - y
    elif op == "*":
        r = x * y
    elif op == "/":
        r = x / y
    elif op == "//":
        r = x // y
    elif op == "%":
        r = x % y
    elif op == "**":
        r = x ** y
    print(x, op, y, "=", r)    

