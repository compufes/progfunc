def valxpeso(l, i):
    if i+1 < len(l):
        return [l[i]*l[i+1]] + valxpeso(l, i+2)
    return []

def somapesos(l, i, j):
    if i+1 < len(l):
        j = l[i+1] + somapesos(l, i+2, j)
    return j

def main():
    f = lambda n: [] if not n else [float(input())] + f(n-1)
    freq = float(input())
    p1 = float(input())
    p2 = float(input())
    mp = (2*p1+3*p2)/5
    ep1 = float(input())
    ep2 = float(input())
    mep = (ep1+ep2)/2
    n = int(input())
    meps = f(2 * n)
    mmep = sum(valxpeso(meps, 0))/somapesos(meps, 0, 0)
    m = int(input())
    listas = f(m)
    ml = sum(listas)/len(listas)
    k = int(input())
    ap = f(k)
    m_ap = sum(ap)/len(ap)
    m = 0.55 * mp + 0.25 * mep + 0.15 * mmep + 0.10 * ml + 0.05 * m_ap
    print("Media (ponderada) das provas: {}".format(round(mp, 1)))
    print("Media dos EPs: {}".format(round(mep, 1)))
    print("Media (ponderada) dos miniEPs: {}".format(round(mmep, 1)))
    print("Media das listas de exercícios: {}".format(round(ml, 1)))
    print("Media das aulas práticas: {}".format(round(m_ap, 1)))
    print("Frequencia: {}%".format(freq))
    print("Media parcial: {}".format(round(m, 1)))
    if 75 <= freq: 
        if 7 <= m:
            print("Media final: {}".format(round(m, 1)))
            print("Aprovado(a) por nota e frequencia.")
        else:
            pf = float(input())
            print("Prova final: {}".format(round(pf, 1)))
            mf = (m + pf) / 2
            print("Media final: {}".format(round(mf, 1)))
            if 5 <= mf:
                print("Aprovado(a) por nota e frequencia.")
            else:
                print("Reprovado(a) por nota.")
    else:
        print("Media final: {}".format(round(m, 1)))
        print("Reprovado(a) por frequencia.")

main()


