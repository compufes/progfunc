def hora_do_tempo_de_entrada(a):
    return a//60

def pega_indice(t, h, m):
    if t<=hora_minuto_pra_minutos(h, m):
       return True 
    else:
        return False 

def minutos_do_tempo_de_entrada(a):
    return a%60

def a_minuto_pra_b(a, b):
    return a*60+b

def f_init():
    tempo=int(input())
    idade=int(input())
    sexo=str(input())
    if sexo=="f"or sexo=="F"or sexo=="M"or sexo=="m":
        if tempo>=0 and idade>=18:
            if sexo=="M"or sexo=="m":
                if 18<=idade and idade<=34:
                    hora_indice,minuto_indice=3,0
                elif 35<=idade and idade<=39:
                    hora_indice,minuto_indice=3,5
                elif 40<=idade and idade<= 44:
                    hora_indice,minuto_indice=3,10
                elif 45<=idade and idade<=49:
                    hora_indice,minuto_indice=3,20
                elif 50<=idade and idade<=54:
                    hora_indice,minuto_indice=3,25
                elif 55<=idade and idade<=59:
                    hora_indice,minuto_indice=3,35
                elif 60<=idade and idade<=64:
                    hora_indice,minuto_indice=3,55
                elif 65<=idade and idade<= 69:
                    hora_indice,minuto_indice=4,5
                elif 70<=idade and idade<=74:
                    hora_indice,minuto_indice =4,20
                elif 75<=idade and idade<=79:
                    hora_indice,minuto_indice=4,35
                elif a >= 80:
                    hora_indice,minuto_indice=4,50
            else:
                if 18<=idade and idade<=34:
                    hora_indice,minuto_indice=3,30
                elif 35<=idade and idade<=39:
                    hora_indice,minuto_indice=3,35
                elif 40<=idade and idade<=44:
                    hora_indice,minuto_indice=3,40
                elif 45<=idade and idade<=49:
                    hora_indice,minuto_indice=3,50
                elif 50<=idade and idade<=54:
                    hora_indice, minuto_indice=3,55
                elif 55<=idade and idade<=59:
                    hora_indice,minuto_indice=4,5
                elif 60<=idade and idade<=64:
                    hora_indice,minuto_indice=4,20
                elif 65<=idade and idade<=69:
                    hora_indice,minuto_indice=4,35
                elif 70<=idade and idade<=74:
                    hora_indice,minuto_indice=4,50
                elif 75<=idade and idade<=79:
                    hora_indice,minuto_indice=5,5
                elif a>=80:
                    hora_indice,minuto_indice=5,20
    return hora_do_tempo_de_entrada(tempo),minutos_do_tempo_de_entrada(tempo),hora_indice,minuto_indice,pega_indice(tempo,hora_indice,minuto_indice),sexo

def f_mens(h,m,h1,m1,f,sexo):
    if(sexo=="m" or sexo=="M"):
        print("Tempo do corredor: {:02}h {:02}min".format(h,m))
    else:
        print("Tempo da corredora: {:02}h {:02}min".format(h,m))
    print("Tempo necessario: {:02}h {:02}min".format(h1,m1))
    l = hora_minuto_pra_minutos(h,m) - hora_minuto_pra_minutos(h1,m1)
    if l<0: l*=(-1)
    if f:
        print("Conseguiu indice? SIM")
        if(sexo=="m" or sexo=="M"):
            print("O corredor correu {:02}h {:02}min abaixo do indice".format(hora_do_tempo_de_entrada(l),minutos_do_tempo_de_entrada(l)))
        else:
            print("A corredora correu {:02}h {:02}min abaixo do indice".format(hora_do_tempo_de_entrada(l),minutos_do_tempo_de_entrada(l)))
    else:
        print("Conseguiu indice? NAO")
        if(sexo=="m" or sexo=="M"):
            print("O corredor correu {:02}h {:02}min acima do indice".format(hora_do_tempo_de_entrada(l),minutos_do_tempo_de_entrada(l)))
        else:
            print("A corredora correu {:02}h {:02}min acima do indice".format(hora_do_tempo_de_entrada(l),minutos_do_tempo_de_entrada(l)))

def comeco(f0,f1):
     a0,a1,a2,a3,a4,a5 = f0()
     f1(a0,a1,a2,a3,a4,a5) 

comeco(f_init, f_mens)


