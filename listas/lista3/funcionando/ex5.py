def f(x, n):
    if not n:
        return 1
    elif not n % 2:
        return f(x**(n/2), n - 1)
    else:
        return f(x**((n-1)/2) * x, n - 1)

print(f(2, 3))



