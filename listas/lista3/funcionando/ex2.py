def fib(n):
    if n == 1 or n == 2:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)

def func(v):
    if v < 0 or type(v) != int:
        print("erro")
        return None
    elif v > 0:
        func(v-1)
        print(fib(v))

func(5)




