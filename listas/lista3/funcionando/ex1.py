def ex1a(n):
    if 10 <= n <= 99:
        d = n // 10
        u = n - 10 * d
        print("{}{}{}{}".format(d,u,u,d))
        ex1a(n+1)

def ex1b(n):
    if n:
        ex1b(n - 1)
        print(n)
    
def ex1c(a, b):
    if a <= b:
        print(a)
        ex1c(a + 1, b)

def ex1d(a, b):
    if b:
        return a * ex1d(a, b - 1)
    return 1

def ex1e(n, i = 2, j = 0):
    if i < n:
        if not n % i:
            j += 1
            ex1e(n, i + 1, j)
    return j + 2

def ex1f(n):
    return True if ex1e(n) == 2 else False

def ex1g_(n, i = 1):
    '''
        m = a // 1000
        c = ((a // 100 * 100) - m * 1000) // 100
        d = ((a // 10 * 10) - (m * 1000 + c * 100)) // 10
        u = a - (m * 1000 + c * 100 + d * 10)
    '''
    u = n // 10 ** i
    x = n - u * 10 ** i
    print(x, end = '')
    if x < 1:
        return ex1g(n, i + 1)

def ex1g(n, i = 1, s = 0):
    a = (n//i)%10
    if a <= 0:
        return s
    else:
        return ex1g(n, i*10, s + a)

ex1a(10)
ex1b(5)
ex1c(1,3)
print(ex1d(5,2))
print(ex1e(4))
print(ex1f(4))
print(ex1f(3))
print(ex1g(3452))
print(ex1g(1234))


