def ehPerfeito(n, i = 1, s = 0):
    if i < n:
        if not n % i:
            s += i
        return ehPerfeito(n, i + 1, s)
    return True if s == n else False 

def perfeitos(n):
    if n and ehPerfeito(n): print(n)
    if n: perfeitos(n - 1)

def perfeitos1(n, f):
    if n and f(n): print(n)
    if n: perfeitos(n - 1)

def main():
    print(ehPerfeito(6))
    print(ehPerfeito(7))
    print(ehPerfeito(28))
    perfeitos(500)

main()


