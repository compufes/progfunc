def ex1a(n = 10):
    if 10 <= n <= 99:
        d = n // 10
        u = n % 10
        # u = n - 10 * d
        print("{}{}{}{}".format(d,u,u,d))
        ex1a(n+1)

def ex1b(n):
    if n:
        ex1b(n - 1)
        print(n)
    
def ex1c(a, b):
    if a <= b:
        print(a)
        ex1c(a + 1, b)

def ex1d(a, b):
    if b:
        return a * ex1d(a, b - 1)
    return 1

def ex1e(n, i = 1, j = 0):
    if i <= n:
        if not n % i:
            j = j + 1
        return ex1e(n, i + 1, j)
    return j

# def ex1f(n):
#     return True if ex1e(n) == 2 else False

# def ex1f(n, i = 2):
#     return True if i == n else False if not n%i else ex1f(n, i + 1)

def ex1f(n, i=2):
    if i < n:
        if not n%i:
            return False
        else:
            ex1f(n, i + 1)
    return True

def ex1g_(n, i = 1):
    '''
        m = a // 1000
        c = ((a // 100 * 100) - m * 1000) // 100
        d = ((a // 10 * 10) - (m * 1000 + c * 100)) // 10
        u = a - (m * 1000 + c * 100 + d * 10)
    '''
    u = n // 10 ** i
    x = n - u * 10 ** i
    print(x, end = '')
    if x < 1:
        return ex1g(n, i + 1)

def ex1g(n, i = 1, s = 0):
    a = (n//i)%10
    print(a)
    if a <= 0:
        return s
    else:
        return ex1g(n, i*10, s + a)

def main():
#    ex1a()
#    ex1b(5)
#    ex1c(4,24)
#    print(ex1d(5,2))
#    print(ex1e(8))
#    print(ex1f(48))
#    print(ex1f(3))
    print(ex1g(3452))
#    print(ex1g(1234))

main()
