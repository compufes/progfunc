from math import factorial

def fact(n):
    return n * fact(n - 1) if n else 1

def euler(n, i = 0):
    if i < n:
        return 1/fact(i) + euler(n, i + 1)
    return 0

def main():
    print("{:.5f}".format(euler(8)))

main()

 
