def fib(n):
    return 1 if n==0 or n == 1 or n == 2 else fib(n - 1) + fib(n - 2)

def func(v):
    if v < 0 or type(v) != int:
        print("erro")
        return None
    elif v > 0:
        func(v-1)
        print(fib(v))

print(func(4))




