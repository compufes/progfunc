def f(n, k):
    if n == k or not k:
        return 1
    elif k == 1:
        return n
    else:
        return f(n-1, k-1) + f(n-1, k)

print(f(6, 2))

 
