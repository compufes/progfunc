def f(n, k):
    if n == k or not k:
        return 1
    elif k == 1:
        return n
    else:
        return f(n-1, k-1) + f(n-1, k)

def g(n, k = 0):
    if -1 < n:
        if k <= n:
            g(n, k + 1)
            print("{} ".format(f(n, k)), end = "")
        else:
            g(n - 1)
            print()

print(g(10))

 
