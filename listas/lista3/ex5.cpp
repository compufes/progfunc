#include <iostream>

using namespace std;

int Exp(int x, int n)
{
  // caso base
	if (!n) return 1;

	auto t = Exp(x, n/2);

	if (n%2)
        return x*t*t; // caso ímpar
  
	return t*t; // caso par
}

int main()
{
    cout << Exp(2, 5) << endl;
}
