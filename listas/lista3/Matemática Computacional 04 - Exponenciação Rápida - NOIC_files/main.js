$(document).ready(() => {

    // =========================================================================
    // PLATFORM DETECTION + IMPORTANT VARIABLES

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
         },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    // =========================================================================
    // GLOBAL VARIABLES

    var winHeight = $(window).height();
    var winWidth = $(window).width();

    updateValues();

    // THROTTLE

    var resizeTimer; // Set resizeTimer to empty so it resets on page load
    var delayedTimer;

    resizeFunction();

    function resizeFunction() {
       // Stuff that should happen on resize
       updateValues();
       updateModel();
    };

    function delayedFunction() {
        delayedUpdate();
    }

    // On resize, run the function and reset the timeout
    // 250 is the delay in milliseconds. Change as you see fit.
    $(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizeFunction, 100);
        delayedTimer = setTimeout(delayedFunction, 500);
    });

    // UPDATE WITH THROTTLE

    function updateValues() {
        winHeight = $(window).height();
        winWidth = $(window).width();

        // =====================================================================
        // SLIDER FRONT-PAGE

        var factor = 1.1;

        if (isMobile.any()) {
            factor = 1;
        }

        if ($(location).attr("href") == "http://www.noic.dev.cc/" || $(location).attr("href") == "http://www.noic.dev.cc/#" || $(location).attr("href") == "http://www.noic.com.br/" || $(location).attr("href") == "http://www.noic.com.br/#" || $(location).attr("href") == "http://noic.com.br/" || $(location).attr("href") == "http://noic.com.br/#" || $(location).attr("href") == "https://noic.com.br/#" || $(location).attr("href") == "https://noic.com.br/" ||
    $(location).attr("href") == "https://www.noic.com.br/" || $(location).attr("href") == "https://www.noic.com.br/#") {

            $('.fullscreen')[0].style.setProperty('height', winHeight * factor - 30 + 'px', 'important');
            $('.fullscreen img')[0].setAttribute("id", "fullscreen-img");

            var imgHeight = document.getElementById('fullscreen-img').clientHeight;
            var imgWidth = document.getElementById('fullscreen-img').clientWidth;

            if (winHeight * imgWidth * factor >= winWidth * imgHeight) {
                $('.fullscreen img')[0].style.setProperty('height', winHeight * factor - 30 + 'px', 'important');
                $('.fullscreen img')[0].style.setProperty('width', 'auto', 'important');
            }
            else {
                $('.fullscreen img')[0].style.setProperty('width', winWidth - 30 + 'px', 'important');
                $('.fullscreen img')[0].style.setProperty('height', 'auto', 'important');
            }
        }

        // =====================================================================
        // HEADER OVERFLOW

        var holder = document.getElementById("holder-id");

        if (holder.offsetWidth >= holder.scrollWidth) {
            // your element doesn't have overflow
            $('.yesspace').css({
                'display':'inherit'
            })
            $('.nospace').css({
                'display':'none'
            })
            $('#holder-id-nospace').css({
                'display':'none'
            })
        }

        if (holder.offsetWidth < holder.scrollWidth) {
            // your element has overflow
            $('.yesspace').css({
                'display':'none'
            })
            $('.nospace').css({
                'display':'inherit'
            })
            $('#holder-id-nospace').css({
                'display':'inherit'
            })
        }

        // =====================================================================
        // PERFECT CIRCLED IMAGES

        $('#pequena .pessoa').css({
            'padding-top': $('#pequena .pessoa').width() + 'px'
        });

        $('#normal .pessoa').css({
            'padding-top': $('.pessoa').width() + 'px'
        });
    }

    // DELAYED UPDATE!

    function delayedUpdate() {
        // =====================================================================
        // PERFECT CIRCLED IMAGES

        $('#pequena .pessoa').css({
            'padding-top': $('#pequena .pessoa').width() + 'px'
        });

        $('#normal .pessoa').css({
            'padding-top': $('.pessoa').width() + 'px'
        });

        if (isMobile.any()) {
            $('.gist').css({
                'padding': '0 1.5rem'
            })
        }
        else {
            $('.gist').css({
                'padding': '0 20vw'
            })
        }
    }

    // =========================================================================
    // MOBILE AND COMPUTER DIFFERENCES

    function updateModel() {
        if (isMobile.any()) {
            $('.pc').css({
                'display': 'none'
            })
            $('.mobile').css({
                'display': 'inherit'
            })

            $('.default-text').css({
                'padding': '0rem 1rem'
            })

            $('.gist, .type-page address, .type-page .wp-block-image, .type-page p, .type-page h2, .type-page h3, .type-page h4, .type-page h5, .type-page h6, .type-page table, .page-gallery, .page-buttons').css({
                'padding': '0 1.5rem'
            })

            $('.sp-wrap').css({
                'margin': '0 1.5rem 10px'
            })

            $('.entry-title').css({
                'padding': '2rem 1.5rem 0'
            })

            $('.type-page ul, .type-page ol').css({
                'margin': '0',
                'padding': '0 1.5rem 0 3rem'
            })

            $('.type-page ul ul, .type-page ol ul, .type-page ul ol, .type-page ol ol').css({
                'margin': '0',
            	'padding': '0 0 0 2rem'
            })

            $('#post-template, .contato').css({
                'padding': '3rem 1.5rem 2rem'
            })
            $('.search-content').css({
                'padding': '2rem 1.5rem 2rem'
            })

            if (winWidth < 500) {
                $('.search-content article .post-thumbnail').css({
                    'width': '100%',
                    'height': 100 / 2 + 'vw',
                    'padding': '0rem 0rem 1rem 0rem'
                })
            }
            else {
                $('.search-content article .post-thumbnail').css({
                    'width': '25vw',
                    'height': '13vw',
                    'padding': '0rem 1.5rem 0 0rem'
                })
            }
        }
        else {
            $('.pc').css({
                'display': 'inherit'
            })
            $('.mobile').css({
                'display': 'none'
            })

            $('.default-text').css({
                'padding': '0rem 8vw'
            })

            $('#post-template, .contato').css({
                'padding': '3rem calc(20vw) 2rem'
            })

            $('.gist, .type-page address, .type-page .wp-block-image, .type-page p, .type-page h2, .type-page h3, .type-page h4, .type-page h5, .type-page h6, .type-page table, .page-gallery, .page-buttons').css({
                'padding': '0 20vw'
            })

            $('.sp-wrap').css({
                'margin': '0 20vw 10px'
            })

            $('.entry-title').css({
                'padding': '4rem 20vw 0'
            })

            $('.type-page ul, .type-page ol').css({
                'margin': '0',
                'padding': '0 20vw 0 calc(20vw + 1rem + 3vw)'
            })

            $('.type-page ul ul, .type-page ol ul, .type-page ul ol, .type-page ol ol').css({
                'margin': '0',
            	'padding': '0 0 0 2rem'
            })

            $('.search-content').css({
                'padding': '4rem calc(10vw) 2rem'
            })

            $('.search-content article .post-thumbnail').css({
                'width': '25vw',
                'height': '13vw',
                'padding': '0rem 1.5rem 0 0rem'
            })
        }
    }

    // =========================================================================
    // SCROLLING EFFECTS

    $(window).scroll(function () {
        var windowpos = $(window).scrollTop();

        $('.divToShowHide').each(function() {
            if (windowpos >= ($(this).offset().top - winHeight * 0.55)) {
                $(this).addClass("after-scroll");
            }
            else {
                $(this).removeClass("after-scroll");
            }
        });
    });
})
