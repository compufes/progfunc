#!/usr/bin/env python

a = int(input())
if 1000 <= a <= 9999:
    m = a // 1000
    c = ((a // 100 * 100) - m * 1000) // 100
    d = ((a // 10 * 10) - (m * 1000 + c * 100)) // 10
    u = a - (m * 1000 + c * 100 + d * 10)

    if m == u and c == d:
        # nao exige saida apenas a verificacao 
        # se eh palidromo
        # print("Palidromo")
        pass
else:
    print("Erro")

