#!/usr/bin/env python

n0, n1 = int(input()), int(input())
n2, n3 = int(input()), int(input())
n_impar, n_par = 0, 0
soma_impar, soma_par = 0, 0

if n0 % 2:
    soma_impar += n0
    n_impar += 1
else:
    soma_par += n0
    n_par += 1

if n1 % 2:
    soma_impar += n1
    n_impar += 1
else:
    soma_par += n1
    n_par += 1

if n2 % 2:
    soma_impar += n2
    n_impar += 1
else:
    soma_par += n2
    n_par += 1

if n3 % 2:
    soma_impar += n3
    n_impar += 1
else:
    soma_par += n3
    n_par += 1

media_impar = soma_impar / n_impar
media_par = soma_par / n_par

# Nao pede saida 
# print("Soma impares {} media {}".format(soma_impar, media_impar))
# print("Soma pares {} media {}".format(soma_par, media_par))
