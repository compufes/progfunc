#!/usr/bin/env python

a = int(input())
if 100 <= a <= 999:
    c = a // 100
    d = ((a // 10 * 10) - c * 100) // 10
    u = a - (c * 100 + d *10)
    
    if c < d < u:
        print("ascendente")
    else:
        print("nao ascendente")

