#!/usr/bin/env python
# -*- coding: utf-8 -*

def main():
    a, b, c = int(input()), int(input()), int(input())

    if a < b:
        a, b, = b, a
    if a < c:
        a , c = c, a

    if a < (b + c):
        if a == b == c:
            res = "equilatero"
        elif a == b or a==c or b==c:
            res = "isosceles"
        else:
            res = "escaleno"
        print("Forma triangulo {}".format(res))

main()
