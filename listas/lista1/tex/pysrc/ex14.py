#!/usr/bin/env python
# -*- coding: utf-8 -*

def main():
    print("Tempo do corredor 1:")
    h0 = int(input("->Quantas horas? "))
    m0 = int(input("->Quantos minutos? "))
    s0 = int(input("->Quantos segundos? "))

    print("Tempo do corredor 2:")
    h1 = int(input("->Quantas horas? "))
    m1 = int(input("->Quantos minutos? "))
    s1 = int(input("->Quantos segundos? "))
    ss0 = s0 + (m0 * 60) + (h0 * 3600)
    ss1 = s1 + (m1 * 60) + (h1 * 3600)

    tt = 0
    if ss0 < ss1:
        print("Vencedor: corredor 1")
        tt = ss1 - ss0
    elif ss0 > ss1:
        print("Vencedor: corredor 2")
        tt = ss0 - ss1
    else:
        print("Empate")

    m = tt // 60
    h = m // 60
    m %= 60
    tt %= 60

    print("Diferença {} horas {} minutos e {} segundos".format(h, m, tt))

main()
