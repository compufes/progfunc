#!/usr/bin/env python

def main():
    ano = int(input("Digite um ano: "))
    a = ano % 19
    b = ano % 4
    c = ano % 7
    if 1581 < ano < 2500:
        if 1582 <= ano <= 1699:
            x, y = 22, 2
        elif 1700 <= ano <= 1799:
            x, y = 23, 3
        elif 1800 <= ano <= 1899:
            x, y = 23, 4
        elif 1900 <= ano <= 1999 or 2000 <= ano <= 2099:
            x, y = 24, 5
        elif 2100 <= ano <= 2199:
            x, y = 24, 6
        elif 2200 <= ano <= 2299:
            x, y = 25, 0
        elif 2300 <= ano <= 2399:
            x, y = 26, 1
        elif 2400 <= ano <= 2499:
            x, y = 25, 1

        d = (19 * a + x) % 30
        e = (2 * b + 4 * c + 6 * d + y) % 7

        p = 22 + d + e
        if p <= 31:
            mes = "Marco"
        else:
            mes = "Abril"
            p = d + e - 9
            if not p <= 25:
                p -= 7
        print("Em {} a Pascoa foi ou sera em {} de {}".format(ano, p, mes))

main()


