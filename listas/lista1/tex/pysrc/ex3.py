#!/usr/bin/env python

tt = int(input("Digite o tempo total gasto na corrida (em min): "))
dist = int(input("Digite a distancia percorrida (em km): "))
tt *= 60
tt /= dist
m = int(tt // 60)
s = int(tt % 60)
print("Ritmo medio do corredor: {:02d}:{:02d} min/km".format(m,s))
