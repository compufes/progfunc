#!/usr/bin/env python
# -*- coding: utf-8 -*

def main():
    a, b = int(input()), float(input())

    if b > 0 and a > 2:
        if a == 3:
            print("Triângulo {}".format(a * b))
        elif a == 4:
            print("Quadrado {}".format(b * b))
        else:
            print("Pentágono")
    else:
        print("Polígono não identificado")


main()


