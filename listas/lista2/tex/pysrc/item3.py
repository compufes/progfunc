#!/usr/bin/env python
# -*- coding: utf-8 -*

from math import log
from math import log10
from math import sqrt
from math import e

def f(x):
    if x <= 1:
        res = log(x)
    elif 1 < x <= 2:
        res = log10(x) + sqrt(x)
    elif 2 < x <= 5:
        res = x ** 2 + e ** x
    elif 5 < x:
        res = x ** (x/2) + log(x, 2)
    print(res)



