def ehTriangulo(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        return False
    if a < b:
        a, b = b, a
    if a < c:
        a, c = c, a
    if b + c <= a:
        return False
    return True

def tipoTriangulo(a, b, c):
    if ehTriangulo(a, b, c):
        if a < (b + c):
            if a == b == c:
                res = "equilatero"
            elif a == b or a==c or b==c:
                res = "isosceles"
            else:
                res = "escaleno"
            print("Forma triangulo {}".format(res))
    else:
        print("Nao forma triangulo")

def main():
    a, b, c = int(input()), int(input()), int(input())
    tipoTriangulo(a, b, c)

main()


