#!/usr/bin/env python
# -*- coding: utf-8 -*

from math import ceil

def f(h, m, s, d):
    ss0 = s + (m * 60) + (h * 3600)
    s = int(ceil(ss0 / d))

    m = s // 60
    s %= 60

    print("Distância percorrida (em km): {}".format(d))
    print("-->Ritmo: {:02d}:{:02d}min/km".format(m, s))

def main():
    h = int(input())
    m = int(input())
    s = int(input())
    d = float(input())
    f(h, m, s, d)

main()
