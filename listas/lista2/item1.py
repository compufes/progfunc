def item_a(a, b):
    if a < b:
        return b
    else:
        return a

from math import sqrt 
def item_b(a):
    return sqrt(a * a) 

def item_c(a):
    return a % 5 == 0

def item_d(a, b, c):
    if a > b:
        a, b = b, a
    if a > c:
        a, c = c, a
    if b > c:
        print(a, c, b)
    else:
        print(a, b, c)

def item_e(a, b, c):
    if a > b:
        a, b = b, a
    if a > c:
        a, c = c, a
    if b > c:
        return a, c, b
    else:
        return a, b, c

from math import pi
def item_f(a):
    area = 4 * pi * a * a
    vol = (4.0/3.0) * pi * a * a * a 
    print(area, vol)
    return area, vol

def item_g(a, b, c):
    if a > b:
        a, b = b, a
    if a > c:
        a, c = c, a
    return b*b + c*c

def item_h(a):
    h = a // 3600
    r = a % 3600
    m = r // 60
    s = r % 60
    print("{:02d}:{:02d}:{:02d}".format(h, m, s))

def item_i(n0, n1, n2, n3):
    n_impar, n_par = 0, 0
    soma_impar, soma_par = 0, 0

    if n0 % 2:
        soma_impar += n0
        n_impar += 1
    else:
        soma_par += n0
        n_par += 1

    if n1 % 2:
        soma_impar += n1
        n_impar += 1
    else:
        soma_par += n1
        n_par += 1

    if n2 % 2:
        soma_impar += n2
        n_impar += 1
    else:
        soma_par += n2
        n_par += 1

    if n3 % 2:
        soma_impar += n3
        n_impar += 1
    else:
        soma_par += n3
        n_par += 1

    media_impar = soma_impar / n_impar
    media_par = soma_par / n_par

    return soma_par, media_par, soma_impar, media_impar

def item_j(a): 
    if type(a) == int:
        print("Inteiro")
    elif type(a) == float:
        print("Ponto Flutuante")
    elif type(a) == bool:
        print("Boleano")
    elif type(a) == str:
        print("String")
    else:
        print("outro tipo")

def item_k(a):
    if type(a) != int or 100 <= a <= 999:
        c = a // 100
        d = ((a // 10 * 10) - c * 100) // 10
        u = a - (c * 100 + d *10)
        
        if c < d < u:
            # print("Ascendente")
            return True
        else:
            False
    else:
        print("erro")
        return False

def item_l(a):
    if type(a) != int or 1000 <= a <= 9999:
        m = a // 1000
        c = ((a // 100 * 100) - m * 1000) // 100
        d = ((a // 10 * 10) - (m * 1000 + c * 100)) // 10
        u = a - (m * 1000 + c * 100 + d * 10)
        if m == u and c == d:
            # nao exige saida apenas a verificacao 
            # se eh palidromo
            # print("Palidromo")
            return True
    else:
        print("Erro")
        return False

def main():
    a, b = 5, 2
    print("a.  Maior {}".format(item_a(a, b)))

    a = -5
    b = 3
    c = 0
    print(item_e(a, b, c))

    a = 3
    item_f(5)
    print(item_g(5, 3, 2))
    print(item_g(5, 8, 2))

    item_h(14476)

    print(item_i(4, 8, 6, 1))

    item_j("S")
    item_j(5.3)
    item_j(print)
    item_j(8)

    item_k(123)

    item_l(443)
main()

