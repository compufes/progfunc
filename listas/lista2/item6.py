def checkProp(a):
    if 1000 <= a <= 9999:
        m = a // 1000
        c = ((a // 100 * 100) - m * 1000) // 100
        d = ((a // 10 * 10) - (m * 1000 + c * 100)) // 10
        u = a - (m * 1000 + c * 100 + d * 10)
        return ((m * 10 + c) + (d * 10 + u)) ** 2 == a
    else:
        return False
def main():
    a = int(input())
    if(checkProp(a)):
        print("Segue a propriedade")
    else:
        print("Nao segue a propriedade")

main()


