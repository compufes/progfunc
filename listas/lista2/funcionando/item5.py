from math import sqrt

def delta(a, b, c):
    return b ** 2 - 4 * a * c

def raizes(a, b, c):
    d = delta(a, b, c)
    if(0 < d):
        print((-b + sqrt(d)) / (2 * a))
        print((-b - sqrt(d)) / (2 * a))

def main():
    a, b, c = int(input()), int(input()), int(input())
    raizes(a, b, c)

main()


