#!/usr/bin/env python

def f(h, m, s, d):
    s += (m * 60)
    s += (h * 3600)
    s /= d
    m = int(s // 60)
    s = int(s % 60)
    print("--> Ritmo: {:02d}:{:02d} min/km".format(m,s))

def main():
    print("Tempo do corredor (horas, minutos e segundos):")
    h = int(input())
    m = int(input())
    s = int(input())
    d = float(input("Distância percorrida (em km): "))
    f(h, m, s, d)

main()


