def f(n, i = 2):
    if n == 1 or i == 0:
        return False
    if i < n:
        if n % i:
            return f(n, i + 1)
        else:
            return False
    return True

print(f(2))


