# def ex1a(l):
#     return not bool(list(filter(lambda x: x == False, map(lambda x: type(x) == int or type(x) == float, l))))

def f1(x):
    if type(x) == int or type(x) == float:
        return True
    else:
        return False

def ex1a(l):
    if False in list(map(f1, l)):
        return False
    else:
        return True

def ex1b(l):
    return sum(l)/len(l)

def ex1c(l, i = 0, k = -1000, g = 1000):
    if i < len(l):
        if l[i] > k:
            k = l[i]
        if l[i] < g:
            g = l[i]
        return ex1c(l, i + 1, k, g)
    return g, k

# def ex1d(l, i = 1):
#     if i <= len(l):
#         return [l[len(l) - i]] + ex1d(l, i + 1)
#     return []

def ex1d(l):
    if l:
        return ex1d(l[1:]) + [l[0]]
    return []

def ex1e(l, x, s = 0):
    if l:
        if x == l[0]:
            s += 1
        return ex1e(l[1:], x, s)
    return s

def ex1f(n, i = 1):
    s = []
    if i <= n:
        if not n % i:
            s = [i]
        return s + ex1f(n, i + 1)
    return []

'''
def primo(n, i = 1, s = 0):
    if i <= n:
        if not n % i:
            s += 1
        return primo(n, i + 1, s)
    return True if s == 2 else False
'''

def primo(n, i = 2): return True if i == n else False if not n%i or n == 1 else primo(n, i + 1)

# def ex2(n, s=[]):
#     if n:
#         if primo(n):
#             s = [n] + s
#         return ex2(n - 1, s)
#     return s

def ex2(n, i = 1, s=[]):
    if len(s) < n:
        if primo(i):
            s += [i]
        return ex2(n, i + 1, s)
    return s

def ex3(l1, l2, l = []):
    if l1:
        if l1[0] in l2 and l1[0] not in l:
            l += [l1[0]]
        return ex3(l1[1:], l2, l)
    return l

def ex4(l, r = []):
    if l:
        if l[0] not in r:
            r += [l[0]]
        return ex4(l[1:], r)
    return r

def ex5(l):
    if len(l) > 1:
        if l[0] < l[1]:
            return ex5(l[1:])
        else:
            return False
    return True

# def ex6(l = [], f = True):
#     if f:
#         i = input("Digite os valores (um numero <0 ou >9 indica o fim da leitura): ")
#         if 0 <= i <=9:
#             l += [i]
#             ex6(l)
#     filter(lambda x: )
 
def main():
    # print(ex1a([2.6, "dfsadfsadf", 1]))
    # print(ex1c([6, 5, 7, 4, 1, 4.2, 7.4]))
    # print(ex1d([1, 2, 3, 4, 5, 6, 7]))
    # print(ex1e([1, 5, 3, 4, 5, 6, 7], 5))
    # print(ex1f(21))
    # print(ex2(8))
    # print(ex3([1, 3, 4, 5, 5, 6], [3, 5, 4, 4]))
    # print(ex4([1, 3, 4, 5, 5, 6]))
    print(ex5([1, 3, 4, 5, 6]))

main()



