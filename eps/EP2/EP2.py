from math import sin

def checkEr(t, er):
    if t/2 < er:
        return True
    else:
        return False

def metodoBisseccao(f, a, b, i = 10, er = 0.005):
    '''
        Metodo da Bisseccao
        f: função
        a-b: faixa do dôminio verificada
        i: números de interações
        er: erro er < |a - b|/2
    '''

    t = b - a if b - a > 0 else a - b

    print("t ", t)

    xi = (a + b) / 2
    print("xi ", xi)

    if not checkEr(t, er) or f(xi) != 0.0 or i:
        if f(a)*f(xi) <= 0:
            print("->a xi ", a, xi)
            metodoBisseccao(a, xi, i - 1, er)
        elif f(xi)*f(b) < 0:
            print("->xi b ", xi, b)
            metodoBisseccao(xi, b, i - 1, er)

def main():
    metodoBisseccao(lambda x: sin(x ** (1/3)), -50, 100)

main()



