from os import system, name
from time import sleep

def print_notas(n100, n50, n20, n10, n5, n2, n1):
    print("n100 {} n50 {} n20 {} n10 {} n5 {} n2 {} n1 {}".format(n100, n50, n20, n10, n5, n2, n1))
    sleep(5)

def analisa_saque(val, n100, n50, n20, n10, n5, n2, n1):
    t100, t50, t20, t10, t5, t2, t1 = distribui(val, n100, n50, n20, n10, n5, n2, n1)

    if n100 < t100 or \
       n50 < t50 or \
       n20 < t20 or \
       n10 < t10 or \
       n5 < t5 or \
       n2 < t2 or \
       n1 < t1: 
           # print("Saldo insuficiente. Temos apenas R${}.00 em caixa.".format(100 * n100 + 50 * n50 + 20 * n20 + 10 * n10 + 5 * n5 + 2 * n2 + n1))
           # r = input("Deseja sacar esse valor (S/N)?")
           # if r == "s" or r == "S":
           #    analisa_saque(100 * n100 + 50 * n50 + 20 * n20 + 10 * n10 + 5 * n5 + 2 * n2 + n1, n100, n50, n20, n10, n5, n2, n1)
           return n100, n50, n20, n10, n5, n2, n1 

    return t100, t50, t20, t10, t5, t2, t1

def sacar(n100, n50, n20, n10, n5, n2, n1):
    print("Notas disponíveis na maquina")
    if n100:
        print("{} x R$100,00".format(n100))
    if n50:
        print("{} x R$50,00".format(n50))
    if n20:
        print("{} x R$20,00".format(n20))
    if n10:
        print("{} x R$10,00".format(n10))
    if n5:
        print("{} x R$5,00".format(n5))
    if n2:
        print("{} x R$2,00".format(n2))
    if n1:
        print("{} x R$1,00".format(n1))

    val = int(input("Digite o valor a ser sacado: R$"))
    n100, n50, n20, n10, n5, n2, n1 = analisa_saque(val, n100, n50, n20, n10, n5, n2, n1)

    return n100, n50, n20, n10, n5, n2, n1

def relatorio(n100, n50, n20, n10, n5, n2, n1):
    print(
"""+---------------------------+
|      Relatório geral      |
+---------------------------+""")
    print("Notas de R$100,00: ", n100)
    print("Notas de  R$50,00: ", n50)
    print("Notas de  R$20,00: ", n20)
    print("Notas de  R$10,00: ", n10)
    print("Notas de   R$5,00: ", n5)
    print("Notas de   R$2,00: ", n2)
    print("Notas de   R$1,00: ", n1)
    print("Saldo total: R${}.00".format(100 * n100 + 50 * n50 + 20 * n20 + 10 * n10 + 5 * n5 + 2 * n2 + n1))
    print("---------------------------")
    input("--> Enter para continuar...")

def saldo(n100, n50, n20, n10, n5, n2, n1):
    print("Saldo atual: R${}.00".format(100 * n100 + 50 * n50 + 20 * n20 + 10 * n10 + 5 * n5 + 2 * n2 + n1))
    input("--> Enter para continuar...")

def distribui(v, n100, n50, n20, n10, n5, n2, n1):
    if n100:
        n = v // 100
        n100 -= n
        if n100 < 0:
            n100 = 0
        if not v % 100:
            return n100, n50, n20, n10, n5, n2, n1
        else:
            v -= n * 100

    if n50:
        n = v // 50
        n50 -= n
        if n50 < 0:
            n50 = 0
        if not v % 50:
            return n100, n50, n20, n10, n5, n2, n1
        else:
            v -= n * 50

    if n20:
        n = v // 20
        n20 -= n
        if n20 < 0:
            n20 = 0
        if not v % 20:
            return n100, n50, n20, n10, n5, n2, n1
        else:
            v -= n * 20

    if n10:
        n = v // 10
        n10 -= n
        if n10 < 0:
            n10 = 0
        if not v % 10:
            return n100, n50, n20, n10, n5, n2, n1
        else:
            v -= n * 10

    if n5:
        n = v // 5
        n5 -= n
        if n5 < 0:
            n5 = 0
        if not v % 5:
            return n100, n50, n20, n10, n5, n2, n1
        else:
            v -= n * 5

    if n2:
        n = v // 2
        n2 -= n
        if n2 < 0:
            n2 = 0
        if not v % 2:
            return n100, n50, n20, n10, n5, n2, n1
        else:
            v -= n * 2

    if n1:
        n = v // 1
        n1 -= n
        if n1 < 0:
            n1 = 0
    return n100, n50, n20, n10, n5, n2, n1

def depositar(n100, n50, n20, n10, n5, n2, n1):
    val = int(input("Coloque o dinheiro: R$"))
    if val == -1:
        return n100, n50, n20, n10, n5, n2, n1
    elif val != 100 and val != 50 and val != 20 and val != 10 and val != 5 and val != 2 and val != 1:
        print("Nota de R${}.00 não reconhecida".format(val));
    else:
        if val == 100:
            n100 += 1
        elif val == 50:
            n50 += 1
        elif val == 20:
            n20 += 1
        elif val == 10:
            n10 += 1
        elif val == 5:
            n5 += 1
        elif val == 2:
            n2 += 1
        elif val == 1:
            n1 += 1
    n100, n50, n20, n10, n5, n2, n1 = depositar(n100, n50, n20, n10, n5, n2, n1)
    return n100, n50, n20, n10, n5, n2, n1

def menuPrincipal():
    limpaTela()
    print("--------------------------------------")
    print("1 - Depositar")
    print("2 - Sacar")
    print("3 - Saldo")
    print("4 - Relatorio")
    print("5 - Finalizar")
    print("--------------------------------------")
    op = int(input("Escolha uma opcao: "))
    if 1 <= op <= 5:
        return op
    else:
        limpaTela()
        menuPrincipal()

def cofrinho(n100 = 1, n50 = 1, n20 = 1, n10 = 1, n5 = 1 , n2 = 1 , n1 = 1):
    op = menuPrincipal()
    if op == 5:
        exit()
    elif op == 1:
        print("-- Para encerrar o depósito digite um valor negativo --")
        n100, n50, n20, n10, n5, n2, n1 = depositar(n100, n50, n20, n10, n5, n2, n1)
    elif op == 2:
        n100, n50, n20, n10, n5, n2, n1 = sacar(n100, n50, n20, n10, n5, n2, n1)
    elif op == 3:
        saldo(n100, n50, n20, n10, n5, n2, n1)
    elif op == 4:
        relatorio(n100, n50, n20, n10, n5, n2, n1)

    cofrinho(n100, n50, n20, n10, n5, n2, n1)

def limpaTela():
    if name == 'nt':
        system('cls')
    else:
        system('clear')

def teste(i = 1):
    limpaTela()
    print("Iteracao: {}".format(i))
    if i >= 5: # Finaliza o programa após 5 iterações
        exit()
    input("--> Enter para continuar...")
    teste(i+1)

def main():
    cofrinho()

main()



