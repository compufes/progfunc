## Arquivos com exercícios da disciplina, Programação Funcional, para 
## para ajudar a quem está cursando esta disciplina

Caso este conteúdo tenha lhe ajudado de alguma forma, peço  
que contribua com suas soluções, sugestões, listas de exercícios
e demais atividades exigidas nesse curso para auxiliar aos outros que precisem.
Favor dar *pull request* ou enviar para:

## compufes.oficial@gmail.com
